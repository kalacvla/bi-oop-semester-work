package cz.cvut.fit.kalacvla

object Main {
  def main(args: Array[String]): Unit = {
    Application.start(args)
  }
}
