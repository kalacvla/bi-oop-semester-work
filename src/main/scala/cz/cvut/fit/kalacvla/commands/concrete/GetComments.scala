package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.constants.Constant.{HACKER_NEWS_API_ITEM_ENDPOINT_API, PATH_TO_COMMENTS, PATH_TO_STORIES}
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.comment.Comment
import cz.cvut.fit.kalacvla.informations.story.Story
import cz.cvut.fit.kalacvla.instructions.Instruction
import cz.cvut.fit.kalacvla.utils.getting.item.GetItem
import cz.cvut.fit.kalacvla.utils.getting.item.impl.GetItemImpl

class GetComments(instruction: Instruction) extends Command {
  //Story's getter
  private val getterStory: GetItem[Story] = new GetItemImpl[Story](PATH_TO_STORIES)
  //Comment's getter
  private val getterComment: GetItem[Comment] = new GetItemImpl[Comment](PATH_TO_COMMENTS)
  //Requested story
  private val story: Option[Story] = getterStory.getItem(instruction.endpoint, HACKER_NEWS_API_ITEM_ENDPOINT_API + instruction.endpoint)
  //Start index of stories
  private val start: Int = (instruction.page - 1) * instruction.pageSize
  //Last index of stories
  private var end: Int = start + instruction.pageSize - 1

  override def execute(): List[Item] = {
    //If does not exist story
    if (story.isEmpty) return items.toList
    else items += story.get
    //Checking if the requested comments are in range
    if (end > story.get.kids.length) end = story.get.kids.length - 1
    //Filling with comments
    completeItems()
    //Return requested comments
    returnResult()
  }

  override def completeItems(): Unit = {
    for (counter <- start to end) {
      val commentId: String = story.get.kids(counter).toString
      val comment: Option[Comment] = getterComment.getItem(commentId, HACKER_NEWS_API_ITEM_ENDPOINT_API + commentId)
      if (comment.isDefined) items += comment.get
    }
  }
}
