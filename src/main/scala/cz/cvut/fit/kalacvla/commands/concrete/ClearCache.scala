package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.constants.Constant.{PATH_TO_COMMENTS, PATH_TO_STORIES, PATH_TO_USERS}
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.cache.Cache
import cz.cvut.fit.kalacvla.utils.FileUtil

object ClearCache extends Command {
  override def execute(): List[Item] = {
    FileUtil.clearFilesInDirectory(PATH_TO_COMMENTS)
    FileUtil.clearFilesInDirectory(PATH_TO_STORIES)
    FileUtil.clearFilesInDirectory(PATH_TO_USERS)
    completeItems()
    returnResult()
  }

  override def completeItems(): Unit = {
    items += Cache(PATH_TO_COMMENTS)
    items += Cache(PATH_TO_STORIES)
    items += Cache(PATH_TO_USERS)
  }
}
