package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.constants.Constant.PATH_TO_USERS
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.user.User
import cz.cvut.fit.kalacvla.instructions.Instruction
import cz.cvut.fit.kalacvla.utils.getting.item.GetItem
import cz.cvut.fit.kalacvla.utils.getting.item.impl.GetItemImpl

class GetUser(instruction: Instruction) extends Command {
  //User's getter
  private val getterUser: GetItem[User] = new GetItemImpl[User](PATH_TO_USERS)

  override def execute(): List[Item] = {
    //Filling with user
    completeItems()
    //Return requested user
    returnResult()
  }

  override def completeItems(): Unit = {
    val userId: String = instruction.endpoint
    val user: Option[User] = getterUser.getItem(userId, userId)
    if (user.isDefined) items += user.get
  }
}
