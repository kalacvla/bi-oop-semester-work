package cz.cvut.fit.kalacvla.commands

import cz.cvut.fit.kalacvla.constants.Constant.NOTHING_FOR_DISPLAYING_MESSAGE
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.utils.Render

import scala.collection.mutable.ListBuffer

trait Command {
  /**
   * Requested items
   */
  var items: ListBuffer[Item] = new ListBuffer[Item]()

  /**
   * Execute the command
   *
   * @return results
   */
  def execute(): List[Item]

  /**
   * Filling requested items to variable "items"
   */
  def completeItems(): Unit

  /**
   * Check if our result is not empty and return it
   *
   * @return requsted items
   */
  def returnResult(): List[Item] = {
    if (items.isEmpty) Render.render(NOTHING_FOR_DISPLAYING_MESSAGE)
    items.toList
  }
}
