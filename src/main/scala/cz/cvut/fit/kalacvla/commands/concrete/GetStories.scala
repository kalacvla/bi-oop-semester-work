package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.constants.Constant.{HACKER_NEWS_API_ITEM_ENDPOINT_API, PATH_TO_STORIES}
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.story.Story
import cz.cvut.fit.kalacvla.instructions.Instruction
import cz.cvut.fit.kalacvla.utils.Parsing
import cz.cvut.fit.kalacvla.utils.api.Api
import cz.cvut.fit.kalacvla.utils.api.impl.HackerNewsApi
import cz.cvut.fit.kalacvla.utils.getting.item.GetItem
import cz.cvut.fit.kalacvla.utils.getting.item.impl.GetItemImpl
import upickle.default._

class GetStories(instruction: Instruction) extends Command {
  // Api for getting stories
  private val api: Api = new HackerNewsApi()
  //Ids of stories
  private val storiesIds: Option[List[Long]] = new Parsing[List[Long]].parsingJson(api.get(instruction.endpoint))
  //Start index of stories
  private val start: Int = (instruction.page - 1) * instruction.pageSize
  //Story' getter
  private val getterStory: GetItem[Story] = new GetItemImpl[Story](PATH_TO_STORIES)
  //Last index of stories
  private var end: Int = start + instruction.pageSize - 1

  override def execute(): List[Item] = {
    //Check if we got id of stories
    if (storiesIds.isEmpty) return List.empty
    //Checking if the requested stories are in range
    if (end > storiesIds.get.length) end = storiesIds.get.length - 1
    //Filling with stories
    completeItems()
    //Return requested stories
    returnResult()
  }

  override def completeItems(): Unit = {
    for (counter <- start to end) {
      val storyId: String = storiesIds.get(counter).toString
      val story: Option[Story] = getterStory.getItem(storyId, HACKER_NEWS_API_ITEM_ENDPOINT_API + storyId)
      if (story.isDefined) items += story.get
    }
  }
}
