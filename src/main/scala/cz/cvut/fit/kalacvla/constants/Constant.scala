package cz.cvut.fit.kalacvla.constants

import scala.util.matching.Regex

object Constant {
  /** Messages */
  final val HELP_MESSAGE: String = "Documentation:" +
    "\n1) Story commands:" +
      "\n\t* top-stories," +
      "\n\t* new-stories," +
      "\n\t* best-stories" +
      "\n\n\tOptional options:" +
        "\n\t\t* --page=\"number\" (Number of page for displaying must be > 0)" +
        "\n\t\t* --page-size=\"number\" (Number of stories in one page must be > 0)" +
    "\n2) User commands:" +
      "\n\t* user" +
      "\n\n\tRequired options:" +
        "\n\t\t* --user-id=\"name of user\"" +
    "\n3) Comments command:" +
      "\n\t* comments" +
      "\n\n\tRequired options" +
        "\n\t\t* --story-id=\"id\" (Story's id for displaying comments)" +
      "\n\tOptional options:" +
        "\n\t\t* --page=\"number\" (Number of page for displaying must be > 0)" +
        "\n\t\t* --page-size=\"number\" (Number of stories in one page must be > 0)" +
    "\n4) Clear cache command:" +
      "\n\t* clear-cache" +
    "\n\nExamples:" +
      "\n\t* hackernewsclient top-stories --page=2 --page-size=20" +
      "\n\t* hackernewsclient user --user-id=Bob" +
      "\n\t* hackernewsclient comments --story-id=127548218" +
      "\n\t* hachernewsclient clear-cache"

  final val NO_ARGUMENTS_MESSAGE: String = "No arguments for application."
  final val WRONG_MESSAGE: String = "Wrong command."
  final val NOTHING_FOR_DISPLAYING_MESSAGE: String = "Nothing for displaying."

  /** Hacker new api */
  final val HACKER_NEWS_API_PREFIX: String = "https://hacker-news.firebaseio.com/v0/"
  final val HACKER_NEWS_API_SUFFIX: String = ".json"
  final val HACKER_NEWS_API_ITEM_ENDPOINT_API: String = "item/"
  final val ENDPOINT_TOP_STORIES: String = "topstories"
  final val ENDPOINT_NEW_STORIES: String = "newstories"
  final val ENDPOINT_BEST_STORIES: String = "beststories"
  final val ENDPOINT_USER: String = "/user/"
  final val ENDPOINT_UPDATES: String = "updates"

  /** Regexes */
  final val REGEX_UNIX_SYMBOLS: Regex = ".*&#x.+;.*".r
  final val REGEX_PAGE: Regex = "--page=.+".r
  final val REGEX_PAGE_SIZE: Regex = "--page-size=.+".r
  final val REGEX_USER_ID: Regex = "--user-id=.+".r
  final val REGEX_STORY_ID: Regex = "--story-id=.+".r

  /** Indexes */
  final val START_INDEX_OF_NUMBER_PAGE: Int = 7
  final val START_INDEX_OF_PAGE_SIZE: Int = 12
  final val START_INDEX_OF_USER_ID: Int = 10
  final val START_INDEX_OF_STORY_ID: Int = 11

  /** Default values */
  final val DEFAULT_PAGE: Int = 1
  final val DEFAULT_PAGE_SIZE: Int = 5
  final val SUFFIX_OF_CACHE_FILE = ".txt"

  /** Commands */
  final val TOP_STORIES_COMMAND: String = "top-stories"
  final val NEW_STORIES_COMMAND: String = "new-stories"
  final val BEST_STORIES_COMMAND: String = "best-stories"
  final val USER_COMMAND: String = "user"
  final val COMMENTS_COMMAND: String = "comments"
  final val CLEAR_CACHE_COMMAND: String = "clear-cache"
  final val HELP_COMMAND: String = "help"
  final val TOP_STORIES_COMMANDv = "top-stories"
  final val SOUGHT_STORY: String = "story"

  /** Pathes */
  final val PATH_TO_STORIES: String = "../cache/stories/"
  final val PATH_TO_COMMENTS: String = "../cache/comments/"
  final val PATH_TO_USERS: String = "../cache/users/"

  /** Patterns */
  final val DATE_FORMAT: String  = "dd.MM.yyyy HH:mm"
}
