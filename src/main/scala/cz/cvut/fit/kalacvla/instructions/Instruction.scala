package cz.cvut.fit.kalacvla.instructions

import cz.cvut.fit.kalacvla.constants.Constant.{DEFAULT_PAGE, DEFAULT_PAGE_SIZE}

case class Instruction(endpoint: String = "", page: Int = DEFAULT_PAGE, pageSize: Int = DEFAULT_PAGE_SIZE, sought: String = "")
