package cz.cvut.fit.kalacvla.utils

import java.text.SimpleDateFormat

import cz.cvut.fit.kalacvla.constants.Constant.{DATE_FORMAT, REGEX_UNIX_SYMBOLS}

import scala.collection.mutable

object Converter {
  //Html sybmols for displaying
  private val regexHTMLSymbols: mutable.Map[String, String] = scala.collection.mutable.Map[String, String](
    ".*&pound;" -> "£",
    "&euro;" -> "€",
    "&trade;" -> "™",
    "&copy;" -> "©",
    "&reg;" -> "®",
    "&quot;" -> "\"",
    "&lt;" -> "<",
    "&gt;" -> ">",
    "<p>" -> "\n\t",
    "<i>" -> "\u001b[3m",
    "</i>" -> "\u001b[0m",
    "<em>" -> "\u001b[3m",
    "</em>" -> "\u001b[0m",
    "<b>" -> "\u001b[1m",
    "</b>" -> "\u001b[0m",
    "<strong>" -> "\u001b[1m",
    "</strong>" -> "\u001b[0m",
  )
  //Html tags for hiding
  private val REGEX_HTML_TAG = ".*<.+>.*".r

  //Converting unix time to human view
  def converteUnixTimeToDate(seconds: Long): String = {
    val ts = seconds * 1000L
    val dateFormate = new SimpleDateFormat(DATE_FORMAT)
    dateFormate.format(ts)
  }

  //Applying html tags
  def converteHTMLSymbols(text: String): String = {
    var changedText: String = converteUnixSymbols(text)
    regexHTMLSymbols.foreach(element => {
      changedText = changedText.replace(element._1, element._2)
    })
    hideHTMLTags(changedText)
  }

  //Converting unix symbols
  def converteUnixSymbols(text: String): String = {
    //Splitting text by \n symbol due to wrong conduct of regex
    val changedText: Array[String] = text.split("\n")
    var result: String = ""

    changedText.foreach(element => {
      if (REGEX_UNIX_SYMBOLS.matches(element)) {
        var tmpText: String = element
        while (REGEX_UNIX_SYMBOLS.matches(tmpText)) {
          val index: Int = tmpText.indexOf("&#x")
          val unixSymbol: String = tmpText.substring(index + 3, index + 5)
          val symbol: Char = Integer.parseInt(unixSymbol, 16).toChar
          tmpText = tmpText.replace("&#x" + unixSymbol + ";", symbol.toString)
        }
        result += tmpText + "\n"
      } else {
        result += element + "\n"
      }
    })
    result.substring(0, result.length() - 1)
  }

  //Hiding html tags
  def hideHTMLTags(text: String): String = {
    //Splitting text by \n symbol due to wrong conduct of regex
    val changedText: Array[String] = text.split("\n")
    var result: String = ""

    changedText.foreach(element => {
      if (REGEX_HTML_TAG.matches(element)) {
        var tmpText: String = element
        while (REGEX_HTML_TAG.matches(tmpText)) {
          val indexFirst = tmpText.indexOf('<')
          val indexLast = tmpText.indexOf('>')
          val textForDeleting: String = tmpText.substring(indexFirst, indexLast + 1)
          tmpText = tmpText.replace(textForDeleting, "")
        }
        result += tmpText + "\n"
      } else {
        result += element + "\n"
      }
    })
    result.substring(0, result.length() - 1)
  }
}
