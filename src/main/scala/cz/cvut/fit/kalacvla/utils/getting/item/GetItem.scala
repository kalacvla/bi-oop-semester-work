package cz.cvut.fit.kalacvla.utils.getting.item

import upickle.default

trait GetItem[T] {
  /**
   * Getting item
   *
   * @param id  of item
   * @param url for request
   * @return item
   */
  def getItem(id: String, url: String)(implicit r: default.Reader[T]): Option[T]
}
