package cz.cvut.fit.kalacvla.utils

import cz.cvut.fit.kalacvla.constants.Constant._
import cz.cvut.fit.kalacvla.instructions.Instruction

object FormationInstruction {
  //For parsing string to int
  private val parser: Parsing[Int] = new Parsing[Int]

  //Creating needed instruction by chosen command
  def execute(args: Array[String]): Option[Instruction] = {
    args.head match {
      case TOP_STORIES_COMMAND => storyInstruction(ENDPOINT_TOP_STORIES, args)
      case NEW_STORIES_COMMAND => storyInstruction(ENDPOINT_NEW_STORIES, args)
      case BEST_STORIES_COMMAND => storyInstruction(ENDPOINT_BEST_STORIES, args)
      case USER_COMMAND => userInstruction(ENDPOINT_USER, args)
      case COMMENTS_COMMAND => commentInstruction(args)
      case CLEAR_CACHE_COMMAND => Some(Instruction(sought = CLEAR_CACHE_COMMAND))
      case _ => None
    }
  }

  /**
   * Creating story instruction
   */
  private def storyInstruction(endpoint: String, args: Array[String]): Option[Instruction] = {
    var page: Int = DEFAULT_PAGE
    var pageSize: Int = DEFAULT_PAGE_SIZE
    for (index <- 1 until args.length) {
      //Looking option with number of page in application's arguments
      //Looking option with number of page size in application's arguments
      if (REGEX_PAGE.matches(args(index))) {
        page = parser.parsingStringToInt(args(index).substring(START_INDEX_OF_NUMBER_PAGE))
        if (page <= 0) return None
      } else if (REGEX_PAGE_SIZE.matches(args(index))) {
        pageSize = parser.parsingStringToInt(args(index).substring(START_INDEX_OF_PAGE_SIZE))
        if (pageSize <= 0) return None
      } else {
        return None
      }
    }
    Some(Instruction(endpoint = endpoint, page = page, pageSize = pageSize, sought = SOUGHT_STORY))
  }

  /**
   * Creating user instruction
   */
  private def userInstruction(endpoint: String, args: Array[String]): Option[Instruction] = {
    if (args.length < 2) return None
    var userId: String = ""
    //Looking user id in application's arguments
    for (index <- 1 until args.length) {
      if (REGEX_USER_ID.matches(args(index))) {
        userId = args(index).substring(START_INDEX_OF_USER_ID)
      } else {
        return None
      }
    }
    Some(Instruction(endpoint = endpoint + userId, sought = USER_COMMAND))
  }

  /**
   * Creating comment instruction
   */
  private def commentInstruction(args: Array[String]): Option[Instruction] = {
    if (args.length < 2) return None

    var page: Int = DEFAULT_PAGE
    var pageSize: Int = DEFAULT_PAGE_SIZE
    var storyId: String = ""

    //Looking option with number of page in application's arguments
    //Looking option with number of page size in application's arguments
    //Looking story's id in application's arguments
    for (index <- 1 until args.length) {
      if (REGEX_PAGE.matches(args(index))) {
        page = parser.parsingStringToInt(args(index).substring(START_INDEX_OF_NUMBER_PAGE))
        if (page <= 0) return None
      } else if (REGEX_PAGE_SIZE.matches(args(index))) {
        pageSize = parser.parsingStringToInt(args(index).substring(START_INDEX_OF_PAGE_SIZE))
        if (pageSize <= 0) return None
      } else if (REGEX_STORY_ID.matches(args(index))) {
        storyId = args(index).substring(START_INDEX_OF_STORY_ID)
      } else {
        return None
      }
    }
    Some(Instruction(storyId, page, pageSize, COMMENTS_COMMAND))
  }
}
