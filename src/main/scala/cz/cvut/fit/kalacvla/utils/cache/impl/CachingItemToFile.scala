package cz.cvut.fit.kalacvla.utils.cache.impl

import cz.cvut.fit.kalacvla.constants.Constant.SUFFIX_OF_CACHE_FILE
import cz.cvut.fit.kalacvla.utils.cache.Caching
import cz.cvut.fit.kalacvla.utils.{FileUtil, Parsing}
import upickle.default

class CachingItemToFile[T] extends Caching[T] {
  private val parser: Parsing[T] = new Parsing[T]

  override def cachingItem(path: String, itemId: String, itemJson: String): Unit = FileUtil.writeToFile(path, itemId + SUFFIX_OF_CACHE_FILE, itemJson)

  override def getItemFromCache(path: String)(implicit r: default.Reader[T]): Option[T] = {
    val jsonItem: String = FileUtil.readFromFile(path)
    parser.parsingJson(jsonItem)
  }
}
