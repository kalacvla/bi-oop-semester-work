package cz.cvut.fit.kalacvla.utils.api.impl

import cz.cvut.fit.kalacvla.constants.Constant.{HACKER_NEWS_API_PREFIX, HACKER_NEWS_API_SUFFIX}
import cz.cvut.fit.kalacvla.utils.api.Api

import scala.io.Source


class HackerNewsApi extends Api {
  override def get(endpoint: String): String = Source.fromURL(HACKER_NEWS_API_PREFIX + endpoint + HACKER_NEWS_API_SUFFIX).mkString
}
