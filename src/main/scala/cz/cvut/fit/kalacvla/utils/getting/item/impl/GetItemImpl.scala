package cz.cvut.fit.kalacvla.utils.getting.item.impl

import cz.cvut.fit.kalacvla.constants.Constant.{ENDPOINT_UPDATES, SUFFIX_OF_CACHE_FILE}
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.updates.Update
import cz.cvut.fit.kalacvla.utils.api.Api
import cz.cvut.fit.kalacvla.utils.api.impl.HackerNewsApi
import cz.cvut.fit.kalacvla.utils.cache.Caching
import cz.cvut.fit.kalacvla.utils.cache.impl.CachingItemToFile
import cz.cvut.fit.kalacvla.utils.getting.item.GetItem
import cz.cvut.fit.kalacvla.utils.{FileUtil, Parsing}
import upickle.default

class GetItemImpl[T](pathToDirectory: String) extends GetItem[T] {
  //Api
  private val api: Api = new HackerNewsApi
  //Cache util
  private val cache: Caching[T] = new CachingItemToFile[T]
  //Cached items T's type
  private val cachedItems: List[String] = FileUtil.getNamesOfFilesFromDirectory(pathToDirectory)
  //Parser items from api
  private val parser: Parsing[T] = new Parsing[T]
  //All items which where updated
  private val updates: Option[Array[String]] = {
    val tmp: Option[Update] = new Parsing[Update].parsingJson(api.get(ENDPOINT_UPDATES))
    if (tmp.isDefined) {
      var upd: Array[String] = tmp.get.profiles
      tmp.get.items.foreach(element => (upd = upd :+ element.toString))
      Some(upd)
    } else {
      None
    }
  }

  override def getItem(id: String, url: String)(implicit r: default.Reader[T]): Option[T] = {
    if (updates.get.contains(id) || !cachedItems.contains(id + SUFFIX_OF_CACHE_FILE)) {
      //Getting item from api
      val item: (Option[T], String) = getItemFromApi(url)
      if (item._1.isDefined) {
        //Caching item
        cache.cachingItem(pathToDirectory, item._1.get.asInstanceOf[Item].getId, item._2)
      }
      item._1
    } else {
      //Getting item from cache
      getItemFromCache(pathToDirectory + id + SUFFIX_OF_CACHE_FILE)
    }
  }

  /**
   * Get item from api
   *
   * @param url to api's endpoint
   * @param r   default reader for parsing
   * @return pair of item and item in json format
   */
  private def getItemFromApi(url: String)(implicit r: default.Reader[T]): (Option[T], String) = {
    val jsonItem: String = api.get(url)
    (parser.parsingJson(jsonItem), jsonItem)
  }

  /**
   * Get item from cache
   *
   * @param path to file
   */
  private def getItemFromCache(path: String)(implicit r: default.Reader[T]): Option[T] = cache.getItemFromCache(path)
}
