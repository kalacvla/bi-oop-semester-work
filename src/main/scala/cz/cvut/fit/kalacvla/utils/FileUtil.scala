package cz.cvut.fit.kalacvla.utils

import java.io.{File, FileNotFoundException, PrintWriter}

import scala.collection.mutable.ListBuffer

object FileUtil {
  /**
   * Get the names of all files from directory
   *
   * @param path to directory
   * @return names of files from directory
   */
  def getNamesOfFilesFromDirectory(path: String): List[String] = {
    try {
      val files: Array[File] = getFilesFromDirectory(path)
      var filesName: ListBuffer[String] = new ListBuffer[String]()
      files.foreach(element => filesName += element.getName)
      filesName.toList
    } catch {
      case _: Throwable => List.empty
    }
  }

  /**
   * Write content into a file
   *
   * @param path     to the file
   * @param fileName name of file
   * @param content  that will be written to the file
   */
  def writeToFile(path: String, fileName: String, content: String): Unit = {
    try {
      val writer = new PrintWriter(path + fileName)
      writer.write(content)
      writer.close()
    } catch {
      case e: FileNotFoundException => {
        val directory = new File(path)
        directory.mkdirs()
        writeToFile(path, fileName, content)
      }
    }
  }

  /**
   * Get content from file
   *
   * @param path to the file
   * @return content of file
   */
  def readFromFile(path: String): String = try {
    scala.io.Source.fromFile(path).mkString
  } catch {
    case _: java.io.FileNotFoundException => ""
  }

  /**
   * Delete all files from directory
   *
   * @param path to the directory
   */
  def clearFilesInDirectory(path: String): Unit = {
    val files: Array[File] = getFilesFromDirectory(path)
    if (files != null) {
      files.foreach(file => deleteFile(path, file.getName))
    }
  }

  /**
   * Get all file that content a directory
   *
   * @param path to directory
   * @return files from directory
   */
  def getFilesFromDirectory(path: String): Array[File] = {
    val directory: File = new File(path)
    directory.listFiles
  }

  /**
   * Delete a file
   *
   * @param path     to directory
   * @param fileName name of file
   */
  def deleteFile(path: String, fileName: String): Unit = {
    val file = new File(path + fileName)
    if (file.delete) Render.render("File " + fileName + " deleted")
    else Render.render("File " + fileName + "not deleted")
  }
}
