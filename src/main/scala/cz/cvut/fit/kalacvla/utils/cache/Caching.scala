package cz.cvut.fit.kalacvla.utils.cache

import upickle.default

trait Caching[T] {
  /**
   * Caching the item
   *
   * @param path     where caching
   * @param itemId   id of item
   * @param itemJson body of item
   */
  def cachingItem(path: String, itemId: String, itemJson: String): Unit

  /**
   * Getting item from cache
   *
   * @param path to file
   * @return item
   */
  def getItemFromCache(path: String)(implicit r: default.Reader[T]): Option[T]
}
