package cz.cvut.fit.kalacvla.utils.api

trait Api {
  /**
   * Http get request
   *
   * @param url for request
   * @return response
   */
  def get(url: String): String
}
