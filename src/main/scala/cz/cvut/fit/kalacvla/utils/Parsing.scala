package cz.cvut.fit.kalacvla.utils


import upickle.default
import upickle.default.read

class Parsing[T] {
  /**
   * Transform json object to scala object
   *
   * @param json object
   * @return scala object
   */
  def parsingJson(json: String)(implicit r: default.Reader[T]): Option[T] = try {
    Some(read[T](json))
  } catch {
    case _: ujson.ParseException => None
    case _: ujson.IncompleteParseException => None
  }

  /**
   * Converting number in string to number in int
   *
   * @param number in string
   * @return number in Int
   */
  def parsingStringToInt(number: String): Int = try {
    number.toInt
  } catch {
    case _: Exception => return -1
  }
}
