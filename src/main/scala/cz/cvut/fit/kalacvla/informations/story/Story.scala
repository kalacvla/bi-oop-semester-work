package cz.cvut.fit.kalacvla.informations.story

import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.utils.Converter
import upickle.default.{macroRW, ReadWriter => RW}

case class Story(
                  id: Long = 0,
                  `type`: String = "",
                  by: String = "",
                  kids: Array[Long] = Array.empty,
                  text: String = "",
                  url: String = "",
                  score: Long = 0,
                  title: String = ""
                ) extends Item {


  override def toString: String = {
    var text: String = ""
    if (url != "") text += title + " (" + url + ")\n" else text += title + "\n"
    if (this.text != "") text += "\t" + Converter.converteHTMLSymbols(this.text) + "\n"
    text += "\t" + score.toString + " points by " + by + "  |  " + kids.length.toString + " comments" + "  |  Item id: " + id.toString
    text
  }

  override def getId: String = id.toString
}

object Story{
  implicit val rw: RW[Story] = macroRW
}

