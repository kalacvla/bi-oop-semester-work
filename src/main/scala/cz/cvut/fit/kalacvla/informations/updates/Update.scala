package cz.cvut.fit.kalacvla.informations.updates

import upickle.default.{macroRW, ReadWriter => RW}

case class Update(items: Array[Long] = Array.empty, profiles: Array[String] = Array.empty)

object Update{
  implicit val rw: RW[Update] = macroRW
}
