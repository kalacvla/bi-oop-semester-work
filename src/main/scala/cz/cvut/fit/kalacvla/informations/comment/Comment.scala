package cz.cvut.fit.kalacvla.informations.comment

import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.utils.Converter
import upickle.default.{macroRW, ReadWriter => RW}

case class Comment(
                    id: Long = 0,
                    by: String = "",
                    text: String = "",
                    time: Long = 0
                  ) extends Item {


  override def toString: String = "* - " + Converter.converteHTMLSymbols(text) + "\n" + "\t" + "By " + by + ", at " + Converter.converteUnixTimeToDate(time) + "\n"

  override def getId: String = id.toString
}

object Comment{
  implicit val rw: RW[Comment] = macroRW
}
