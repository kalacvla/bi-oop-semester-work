package cz.cvut.fit.kalacvla.informations.cache

import cz.cvut.fit.kalacvla.informations.Item

case class Cache(path: String) extends Item {
  override def toString: String = "The path \"" + path + "\" has been cleared"

  override def getId: String = ""
}
