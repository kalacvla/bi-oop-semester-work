package cz.cvut.fit.kalacvla.informations.user

import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.utils.Converter
import upickle.default.{macroRW, ReadWriter => RW}

case class User(
                 id: String = "",
                 about: String = "",
                 created: Long = 0,
                 karma: Long = 0,
                 submitted: Array[Long] = Array.empty
               ) extends Item {

  override def toString: String = {
    var text: String = "User: " + id + "\n"
    if (about != "") text += about + "\n"
    text += "\tCreated at: " + Converter.converteUnixTimeToDate(created) + " | " + "Karma: " + karma.toString + " | " + submitted.length.toString + " submitted items\n"
    text
  }

  override def getId: String = id
}

object User{
  implicit val rw: RW[User] = macroRW
}
