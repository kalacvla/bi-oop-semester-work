package cz.cvut.fit.kalacvla

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.commands.concrete.{ClearCache, GetComments, GetStories, GetUser}
import cz.cvut.fit.kalacvla.constants.Constant._
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.instructions.Instruction
import cz.cvut.fit.kalacvla.utils.{FormationInstruction, Render}

object Application {

  /**
   * Start application
   *
   * @param args of application
   */
  def start(args: Array[String]): Unit = {
    if (args.length == 0) {
      Render.render(NO_ARGUMENTS_MESSAGE + " " + HELP_MESSAGE)
    } else {
      if (args.head == HELP_COMMAND) {
        Render.render(HELP_MESSAGE)
        return
      }
      val instruction: Option[Instruction] = FormationInstruction.execute(args)
      if (instruction.isDefined) {
        execute(instruction.get)
      } else {
        Render.render(WRONG_MESSAGE + " " + HELP_MESSAGE)
      }
    }
  }

  /**
   * Performing a task according to instructions
   *
   * @param instruction for execution
   */
  private def execute(instruction: Instruction): Unit = {
    val command: Option[Command] = instruction.sought match {
      case SOUGHT_STORY => Some(new GetStories(instruction))
      case USER_COMMAND => Some(new GetUser(instruction))
      case COMMENTS_COMMAND => Some(new GetComments(instruction))
      case CLEAR_CACHE_COMMAND => Some(ClearCache)
      case _ => None
    }
    if (command.isDefined) {
      val items: List[Item] = command.get.execute()
      items.foreach(item => Render.render(item.toString))
    }
  }
}
