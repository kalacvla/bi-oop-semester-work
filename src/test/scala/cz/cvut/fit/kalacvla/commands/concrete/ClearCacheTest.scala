package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.constants.Constant.{PATH_TO_COMMENTS, PATH_TO_STORIES, PATH_TO_USERS}
import cz.cvut.fit.kalacvla.utils.FileUtil
import org.scalatest.FunSuite

class ClearCacheTest extends FunSuite {
  test("Test ze po commande clear cache budou slozky prazdne") {
    ClearCache.execute()
    val directoryStories: List[String] = FileUtil.getNamesOfFilesFromDirectory(PATH_TO_STORIES)
    val directoryComments: List[String] = FileUtil.getNamesOfFilesFromDirectory(PATH_TO_COMMENTS)
    val directoryUser: List[String] = FileUtil.getNamesOfFilesFromDirectory(PATH_TO_USERS)
    assert(directoryUser.length === 0)
    assert(directoryComments.length === 0)
    assert(directoryStories.length === 0)
  }
}
