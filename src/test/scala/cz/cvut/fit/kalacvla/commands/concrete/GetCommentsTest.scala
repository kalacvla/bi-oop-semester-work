package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.comment.Comment
import cz.cvut.fit.kalacvla.informations.story.Story
import cz.cvut.fit.kalacvla.instructions.Instruction
import org.scalatest.FunSuite

class GetCommentsTest extends FunSuite {
  test("Test that we get 5 items, first is story and 4 comments, story has id 126809") {
    val instruction: Instruction = Instruction(endpoint = "126809", 1, 4)
    val getComments: Command = new GetComments(instruction)
    val items: List[Item] = getComments.execute()
    assert(items.length === 5)
    assert(items.head.isInstanceOf[Story])
    assert(items.head.getId === "126809")
    for (index <- 1 until items.length) {
      assert(items(index).isInstanceOf[Comment])
    }
  }
}
