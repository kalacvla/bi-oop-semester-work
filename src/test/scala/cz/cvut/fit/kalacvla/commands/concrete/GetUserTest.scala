package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.user.User
import cz.cvut.fit.kalacvla.instructions.Instruction
import org.scalatest.FunSuite

class GetUserTest extends FunSuite {
  test("Test that we get just one item, this item have id (jl) and item is user") {
    val instruction: Instruction = Instruction(endpoint = "/user/jl")
    val getUser: Command = new GetUser(instruction)
    val result: List[Item] = getUser.execute()
    assert(result.length === 1)
    assert(result.head.isInstanceOf[User])
    assert(result.head.getId === "jl")
  }
}
