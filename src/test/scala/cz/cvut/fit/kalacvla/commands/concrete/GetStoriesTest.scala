package cz.cvut.fit.kalacvla.commands.concrete

import cz.cvut.fit.kalacvla.commands.Command
import cz.cvut.fit.kalacvla.informations.Item
import cz.cvut.fit.kalacvla.informations.story.Story
import cz.cvut.fit.kalacvla.instructions.Instruction
import org.scalatest.FunSuite

class GetStoriesTest extends FunSuite {
  test("Test that we get exactly 5 items, and that the items are stories and have not \"default\" id,") {
    val instruction: Instruction = Instruction(endpoint = "topstories", 2, 5)
    val getStories: Command = new GetStories(instruction)
    val items: List[Item] = getStories.execute()
    assert(items.length === 5)
    items.foreach(element => {
      assert(element.isInstanceOf[Story])
      assert(element.getId !== "0")
    })
  }
}
