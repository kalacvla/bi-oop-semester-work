package cz.cvut.fit.kalacvla.informations.user

import org.scalatest.FunSuite

class UserTest extends FunSuite {
  test("Test info") {
    val user: User = User("Tom", "I am programmer", 1, 1000, Array.empty)
    assert(user.toString === "User: Tom\nI am programmer\n\tCreated at: 01.01.1970 01:00 | Karma: 1000 | 0 submitted items\n")
  }
}
