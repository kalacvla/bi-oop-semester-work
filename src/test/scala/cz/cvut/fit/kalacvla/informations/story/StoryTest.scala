package cz.cvut.fit.kalacvla.informations.story

import org.scalatest.FunSuite

class StoryTest extends FunSuite {
  test("Test info") {
    val story: Story = Story(id = 99, `type` = "top", by = "Tom", kids = Array.empty, text = "TEXT", url = "this.is.url", score = 999, title = "This is title")
    assert(story.toString === "This is title (this.is.url)\n\tTEXT\n\t999 points by Tom  |  0 comments  |  Item id: 99")
  }
}
