package cz.cvut.fit.kalacvla.informations.cache

import org.scalatest.FunSuite

class CacheTest extends FunSuite {
  test("Test info") {
    val cache: Cache = Cache("../cache")
    assert(cache.toString === "The path \"../cache\" has been cleared")
  }
}
