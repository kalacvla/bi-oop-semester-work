package cz.cvut.fit.kalacvla.informations.comment


import org.scalatest.FunSuite

class CommentTest extends FunSuite {
  test("Test info") {
    val comment: Comment = Comment(id = 12345, by = "Tom", text = "Hello <a href\"google.com\">World!</a>", time = 1)
    assert(comment.toString === "* - Hello World!\n\tBy Tom, at 01.01.1970 01:00\n")
  }
}
