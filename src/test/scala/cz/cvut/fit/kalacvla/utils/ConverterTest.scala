package cz.cvut.fit.kalacvla.utils
import org.scalatest.FunSuite

class ConverterTest extends FunSuite {
  test("Converte the unix 1 second to human time") {
    val result: String = Converter.converteUnixTimeToDate(1)
    assert(result === "01.01.1970 01:00")
  }
  test("Converte unix symbol to human symbol") {
    val text: String = "It&#x27;s test"
    val result: String = Converter.converteUnixSymbols(text)
    assert(result === "It's test")
  }
  test("Apply html tags for text") {
    val text: String = "<i>Hello <strong>World</strong>!</i>"
    val result: String = Converter.converteHTMLSymbols(text)
    assert(result === "\u001b[3mHello \u001b[1mWorld\u001b[0m!\u001b[0m")
  }
  test("Hiding html tags") {
    val text: String = "If you want to find something into internet, use <a href=\"https://www.google.com/\">Google</a>"
    val result: String = Converter.hideHTMLTags(text)
    assert(result === "If you want to find something into internet, use Google")
  }
}
