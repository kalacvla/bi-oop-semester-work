package cz.cvut.fit.kalacvla.utils

import org.scalatest.FunSuite

class FileUtilTest extends FunSuite {
  test("Test method for getting files from directory") {
    val result: Int = FileUtil.getFilesFromDirectory("./").length
    assert(result !== 0)
  }

  test("Test method for getting names of files from directory") {
    val filesInRootOfProject: Array[String] = Array("target", ".bsp", "project", ".gitignore", ".git", "build.sbt", ".idea", "src")
    val result: List[String] = FileUtil.getNamesOfFilesFromDirectory("./")
    assert(result === filesInRootOfProject)
  }

  test("Test method for writing info to file") {
    FileUtil.writeToFile("../cache/test/", "test.txt", "Hello World!")
    val result: String = scala.io.Source.fromFile("../cache/test/test.txt").mkString
    assert(result === "Hello World!")
  }

  test("Test method for reading info from file") {
    FileUtil.writeToFile("../cache/test/", "test.txt", "Hello World!")
    val result: String = FileUtil.readFromFile("../cache/test/test.txt")
    assert(result === "Hello World!")
  }

  test("Test method for deleting file and method for deleting files from directory") {
    FileUtil.clearFilesInDirectory("../cache/test/")
    val filesInDirectory = FileUtil.getNamesOfFilesFromDirectory("../cache/test/")
    assert(filesInDirectory.length === 0)
  }
}
