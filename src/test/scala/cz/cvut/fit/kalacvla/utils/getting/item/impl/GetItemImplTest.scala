package cz.cvut.fit.kalacvla.utils.getting.item.impl

import cz.cvut.fit.kalacvla.constants.Constant.PATH_TO_USERS
import cz.cvut.fit.kalacvla.informations.user.User
import cz.cvut.fit.kalacvla.utils.getting.item.GetItem
import org.scalatest.FunSuite

class GetItemImplTest extends FunSuite {
  test("Test getting user") {
    val getItem: GetItem[User] = new GetItemImpl[User](PATH_TO_USERS)
    val user: Option[User] = getItem.getItem("jl", "/user/jl")
    assert(user.isDefined)
    assert(user.get.id === "jl")
  }
}
