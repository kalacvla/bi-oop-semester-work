package cz.cvut.fit.kalacvla.utils.api.impl

import cz.cvut.fit.kalacvla.utils.api.Api
import org.scalatest.FunSuite


class HackerNewsApiTest extends FunSuite {
  test("Test get method from Hacker New API") {
    val api: Api = new HackerNewsApi()
    val response: String =  api.get("/user/jl")
    assert(response.contains("\"id\":\"jl\""))
  }
}
