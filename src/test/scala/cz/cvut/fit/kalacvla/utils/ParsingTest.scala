package cz.cvut.fit.kalacvla.utils

import org.scalatest.FunSuite

class ParsingTest extends FunSuite {
  test( "Test parsing json to boolean object") {
    val parsing: Parsing[Boolean] = new Parsing[Boolean]
    val result: Option[Boolean] = parsing.parsingJson("true")
    assert(result.get === true)
  }

  test("Parsing string to int") {
    val parsing: Parsing[Int] = new Parsing[Int]
    val result: Int = parsing.parsingStringToInt("123")
    assert(result === 123)
  }
}
