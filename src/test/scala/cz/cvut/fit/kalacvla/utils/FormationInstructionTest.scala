package cz.cvut.fit.kalacvla.utils

import cz.cvut.fit.kalacvla.constants.Constant._
import cz.cvut.fit.kalacvla.instructions.Instruction
import org.scalatest.FunSuite

class FormationInstructionTest extends FunSuite {
  test("Create instruction based on info") {
    val args: Array[String] = Array(CLEAR_CACHE_COMMAND)
    val result:Option[Instruction] = FormationInstruction.execute(args)
    assert(result.get === Instruction("", DEFAULT_PAGE, DEFAULT_PAGE_SIZE, CLEAR_CACHE_COMMAND))
  }
  test("Test creating stories instruction") {
    val args: Array[String] = Array("top-stories", "--page=9", "--page-size=45")
    val result:Option[Instruction] = FormationInstruction.execute(args)
    assert(result.get === Instruction(ENDPOINT_TOP_STORIES, 9, 45, SOUGHT_STORY))
  }
  test("Test creating user instruction") {
    val args: Array[String] = Array("user", "--user-id=Bob")
    val result:Option[Instruction] = FormationInstruction.execute(args)
    assert(result.get === Instruction(ENDPOINT_USER + "Bob", DEFAULT_PAGE, DEFAULT_PAGE_SIZE, USER_COMMAND))
  }

  test("Test creating comments instruction") {
    val args: Array[String] = Array("comments", "--story-id=128745", "--page=9", "--page-size=45")
    val result:Option[Instruction] = FormationInstruction.execute(args)
    assert(result.get === Instruction("128745", 9, 45, COMMENTS_COMMAND))
  }
}
