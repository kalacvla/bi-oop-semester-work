package cz.cvut.fit.kalacvla.instructions

import cz.cvut.fit.kalacvla.constants.Constant.DEFAULT_PAGE
import org.scalatest.FunSuite

class InstructionTest extends FunSuite{

  test("Check default contsructor") {
    val instruction: Instruction = Instruction()
    assert(instruction.page === DEFAULT_PAGE)
  }
}
