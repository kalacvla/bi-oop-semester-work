name := "HackerNews"

version := "0.1"

scalaVersion := "2.13.3"
libraryDependencies += "com.lihaoyi" %% "upickle" % "0.9.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test

assemblyJarName in assembly := "HackerNewsAPI.jar"
